from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_solve, diplomacy_eval, diplomacy_print, diplomacy_read

class TestDiplomacy (TestCase):

    # ----
    # read 
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        t = diplomacy_read(s)
        self.assertEqual(t,  ['A', 'Madrid', 'Hold'])
    
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Support', 'A'], ['C', 'Paris', 'Support', 'A']])
        self.assertEqual(v, {'A' : 'Madrid', 'B': 'Barcelona', 'C': 'Paris'})

    def test_eval_2(self):
        v = diplomacy_eval([['B', 'Barcelona', 'Support', 'A'], ['A', 'Madrid', 'Hold'], ['C', 'Paris', 'Support', 'A']])
        self.assertEqual(v, {'A' : 'Madrid', 'B': 'Barcelona', 'C': 'Paris'})
    
    def test_eval_3(self):
        v = diplomacy_eval([['B', 'Barcelona', 'Move', 'Madrid'], ['A', 'Madrid', 'Hold'], ['C', 'Paris', 'Support', 'A']])
        self.assertEqual(v, {'A' : 'Madrid', 'B': '[dead]', 'C': 'Paris'})
    
    def test_eval_4(self):
        v = diplomacy_eval([['D', 'Paris', 'Support', 'C'], ['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Paris'], 
        ['C', 'London', 'Support', 'A']])
        self.assertEqual(v, {'A' : 'Madrid', 'B': '[dead]', 'C': 'London', 'D': '[dead]'})

    def test_eval_5(self):
        v = diplomacy_eval([['D', 'Paris', 'Support', 'C'], ['A', 'Madrid', 'Move', 'Houston'], ['B', 'Barcelona', 'Support', 'A'], 
        ['C', 'London', 'Support', 'A']])
        self.assertEqual(v, {'A' : 'Houston', 'B': 'Barcelona', 'C': 'London', 'D': 'Paris'})

    def test_eval_6(self):
        v = diplomacy_eval([["A", "Paris", "Support", "B"], ["B", "London", "Support", "C"], ["C", "Barcelona", "Support", "A"]])
        self.assertEqual(v, {"A" : "Paris", "B" : "London", "C" : "Barcelona"})
        
    def test_eval_7(self):
        v = diplomacy_eval([["A", "Paris", "Hold"], ["C", "Barcelona", "Move", "London"], ["B", "London", "Support", "A"],
        ["D", "Madrid", "Support", "B"], ["E", "NewYork", "Support", "C"], ["F", "Houston", "Move", "NewYork"]])
        self.assertEqual(v, {"A" : "Paris", "B" : "London", "C" : "[dead]", "D" : "Madrid", "E": "[dead]", "F": "[dead]"})

    def test_eval_8(self):
        v = diplomacy_eval([["A", "Paris", "Hold"], ["E", "NewYork", "Support", "C"], ["C", "Barcelona", "Move", "London"], 
        ["B", "London", "Support", "A"], ["D", "Madrid", "Support", "B"], ["F", "Houston", "Move", "NewYork"]])
        self.assertEqual(v, {"A" : "Paris", "B" : "London", "C" : "[dead]", "D" : "Madrid", "E": "[dead]", "F": "[dead]"})

    def test_eval_9(self):
        v = diplomacy_eval([["A", "Paris", "Move", "Barcelona"], ["B", "Barcelona", "Move", "Paris"], ["C", "Houston", "Support", "A"],
        ["D", "NewYork", "Support", "B"]])
        self.assertEqual(v, {"A" : "Barcelona", "B" : "Paris", "C" : "Houston", "D" : "NewYork"})

    def test_eval_10(self):
        v = diplomacy_eval([["A", "Paris", "Move", "Barcelona"], ["E", "NewYork", "Support", "C"], ["C", "Barcelona", "Hold"], 
        ["B", "London", "Support", "A"], ["D", "Madrid", "Support", "B"], ["F", "Houston", "Move", "NewYork"]])
        self.assertEqual(v, {"A" : "Barcelona", "B" : "London", "C" : "[dead]", "D" : "Madrid", "E": "[dead]", "F": "[dead]"})

    def test_eval_11(self):
        v = diplomacy_eval([["I", "Sydney", "Move", "NewYork"], ["A", "Paris", "Move", "Barcelona"], ["B", "Barcelona", "Hold"], 
        ["C", "Austin", "Support", "A"], ["D", "London", "Support", "B"], ["E", "Madrid", "Support", "D"], ["F", "Houston", "Support", "E"], 
        ["G", "NewYork", "Support", "C"], ["H", "Istanbul", "Support", "G"], ["J", "Beijing", "Support" ,"B"]])
        self.assertEqual(v, {"A" : "[dead]", "B" : "Barcelona", "C" : "[dead]", "D" : "London", "E": "Madrid", "F": "Houston", "G": "NewYork",
        "H": "Istanbul", "I": "[dead]", "J": "Beijing"})

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, {'A': '[dead]'})
        self.assertEqual(w.getvalue(), "A [dead]\n")
    
    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Paris Support A\nE NewYork Hold\nF Austin Move NewYork\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE [dead]\nF [dead]\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB [dead]\nC [dead]\nD Paris\n")
    
    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Support C\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\nD Austin\n")

    # corner case where one army is supporting a supporter but that supporter is interfered
    def test_solve_4(self): # in this situation army C is interefered
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\nD Paris Support C\nE Austin Move London\nF Houston Support C\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC London\nD Paris\nE [dead]\nF Houston\n")

    # coverage wants an empty line
    def test_solve_5(self):
        r = StringIO("\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "\n")
# ----
# main
# ----

if __name__ == "__main__":  #pragma: no cover
    main()