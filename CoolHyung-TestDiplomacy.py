#Unit Tests

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve


# -----------
# TestCollatz
# -----------


class TestDiplomacy(TestCase):
    # ----
    # read
    # ----
    def test_read_1(self):
        s = "A Madrid Hold\nB Move Barcelona Madrid\nC Support B\n"
        i = diplomacy_read(s)
        self.assertEqual(i, [['A','Madrid','Hold'],['B','Move','Barcelona','Madrid'], ['C','Support','B']] )

    # ----
    # eval
    # ----
    
    def test_eval_1(self):
        x = diplomacy_eval([['A','Madrid','Hold'],['B','Barcelona','Move','Madrid'],['C','London','Support','B']])
        self.assertEqual(x, ['A', '[dead]','B','Madrid','C','London'])
    

    # -----
    # print
    # -----
    

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, ['A','[dead]','B','Madrid','C','London'])
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")
    

    # -----
    # solve
    # -----
    
    def test_solve1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n D Seoul Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
    
# ----
# main
# ----

if __name__ == "__main__":
    main()
