
# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_eval, diplomacy_solve

class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "A Madrid Hold\n"
        i = diplomacy_read(s)
        self.assertEqual(i[0],  "A")
        self.assertEqual(i[1],  "Madrid")
        self.assertEqual(i[2],  ["Hold", ""])
    
    def test_read_2(self):
        s = "B Barcelona Move Madrid\n"
        i = diplomacy_read(s)
        self.assertEqual(i[0],  "B")
        self.assertEqual(i[1],  "Barcelona")
        self.assertEqual(i[2],  ["Move","Madrid"])
    
    def test_read3(self):
        s = "C London Support B\n"
        i = diplomacy_read(s)
        self.assertEqual(i[0],  "C")
        self.assertEqual(i[1],  "London")
        self.assertEqual(i[2],  ["Support","B"])


    def test_read4(self):
        s = "D Austin Move London\n"
        i = diplomacy_read(s)
        self.assertEqual(i[0],  "D")
        self.assertEqual(i[1],  "Austin")
        self.assertEqual(i[2],  ["Move","London"])

    # ----
    # print
    # ----
    def test_print(self):
        w = StringIO()
        d = {"A":"Madrid"}
        diplomacy_print(w, d )
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print2(self):
        w = StringIO()
        d = {"A":"[Dead]", "B":"[Dead]" }
        diplomacy_print(w, d)
        self.assertEqual(w.getvalue(), "A [Dead]\nB [Dead]\n")

    def test_print3(self):
        w = StringIO()
        d = {"A":"[Dead]", "B":"[Dead]", "C":"Austin" }
        diplomacy_print(w, d)
        self.assertEqual(w.getvalue(), "A [Dead]\nB [Dead]\nC Austin\n")

    def test_print4(self):
        w = StringIO()
        d = {"A":"NewYork", "B":"[Dead]", "C":"Austin" }
        diplomacy_print( w, d )
        self.assertEqual(w.getvalue(), "A NewYork\nB [Dead]\nC Austin\n")

    # ----
    # eval
    # ----
    def test_eval(self):
        a = a = [ ["A", "Madrid", ["Hold", ""] ], ["B", "Austin", ["Hold", ""] ]  ]
        b = {'A': 'Madrid', 'B': 'Austin'}
        c = diplomacy_eval(a)
        self.assertEqual(c,b)

    def test_eval2(self):
        a = [ ["A", "Madrid", ["Hold", ""] ], ["B", "Barcelona", ["Move", "Madrid"]], ["C", "London", ["Support", "B"]] ]
        b = {'A': '[Dead]', 'B': 'Madrid', 'C': 'London'}
        c = diplomacy_eval(a)
        self.assertEqual(c,b)

    def test_eval3(self):
        a = [ ["A", "Madrid", ["Hold", ""] ], ["B", "Barcelona", ["Move", "Madrid"]]  ]
        b = {'A': '[Dead]', 'B': '[Dead]'}
        c = diplomacy_eval(a)
        self.assertEqual(c,b)

    def test_eval4(self):
        a = [ ["A", "Madrid", ["Hold", ""] ], ["B", "Barcelona", ["Move", "Madrid"]], ["C", "London", ["Move", "Madrid"]] , ["D", "Paris", ["Support", "B"]] ]
        b = {'A': '[Dead]', 'B': 'Madrid', 'C': '[Dead]', 'D': 'Paris'}
        c = diplomacy_eval(a)
        self.assertEqual(c,b)
    
    # ----
    # solve
    # ----
    def test_solve(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [Dead]\nB [Dead]\n")

    def test_solve3(self):
        r = StringIO("A Austin Hold\nB Dallas Move Austin\nC FortWorth Support B\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [Dead]\nB Austin\nC FortWorth\n")

    def test_solve4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [Dead]\nB Madrid\nC [Dead]\nD Paris\n")

if __name__ == "__main__": #pragma: no cover
    main()